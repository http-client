import React from 'react';
import App from './app.jsx';
import Manager from './manager.js';

let props = {
  manager: new Manager('http://localhost:8088')
};

window.addEventListener(
  'load', () => React.render(<App {...props} />, document.body)
);
