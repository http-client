export default {
  _eventHandlers: {},

  on(type, handler) {
    if(! (type in this._eventHandlers)) {
      this._eventHandlers[type] = [];
    }
    this._eventHandlers[type].push(handler)
  },

  trigger(type, data) {
    var handlers = this._eventHandlers[type] || [];
    var event = { type: type, data: data };
    console.log('TRIGGER', this, type, handlers.length);
    handlers.forEach((handler) => handler(event));
  },

  forwardEvent(source, type) {
    source.on(type, (event) => this.trigger(type, event.data));
  }

}
