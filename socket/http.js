
import TCPSocket from './tcp';
import LineBuffer from './line_buffer';
import StateMachine from './state_machine';
import EventEmitter from './event_emitter';

export class Connection {
  constructor(host, port, options) {
    Object.assign(this, StateMachine);
    Object.assign(this, EventEmitter);

    this.defineStates({
      init: ['open', 'closed'],
      open: ['busy', 'closed'],
      busy: ['open', 'closed'],
      closed: ['open']
    });

    this.parser = new Parser();
    this.forwardEvent(this.parser, 'message');

    this.serializer = new Serializer();

    this.socket = new TCPSocket(host, port, options);
    this.socket.on('data', (event) => {
      this.parser.push(event.data);
    });
    this.socket.on('close', (event) => this.jump('closed'));
    this.socket.on('open', (event) => this.jump('open'));
  }

  send(message) {
    this.socket.write(this.serializer.encode(message));
  }
}

class Parser {
  constructor() {
    Object.assign(this, StateMachine);
    Object.assign(this, EventEmitter);

    this.defineStates({
      init: ['headers'],
      headers: ['body', 'chunked'],
      body: ['finished'],
      chunked: ['finished'],
      finish: ['init']
    });

    this.lineBuffer = new LineBuffer();
    this.lineBuffer.on('line', (line) => this.processLine(line));
  }

  push(data) {
    if(this.state === 'init' || this.state === 'headers') {
      this.lineBuffer.push(data);
    } else {
      this.bodyBuffer += data;
    }
  }

  processLine(line) {
    if(this.state === 'init') {
      this.currentMessage = this.parseFirstLine(line);
      this.jump('headers');
    } else if(this.state === 'headers') {
      let header = this.parseHeaderLine(line);
      if(header) {
        this.currentMessage.headers.push(header);
      } else {
        this.jump('body');
      }
    } else {
      throw new Error("Invalid state for processLine: " + this.state);
    }
  }

  parseFirstLine(line) {
    return this.parseRequestHead(line) ||
           this.parseResponseHead(line);
  }

  parseRequestHead(line) {
    let requestMatch = line.match(/^([A-Z]+)\s([^\s]+)\sHTTP\/(\d\.\d)$/);
    if(requestMatch) {
      return {
        type: 'request',
        method: requestMatch[1],
        uri: requestMatch[2],
        version: requestMatch[3]
      }
    }
  }

  parseResponseHead(line) {
    let responseMatch = line.match(/^HTTP\/(\d\.\d) (\d+) (.*)$/);
    if(responseMatch) {
      return {
        type: 'response',
        status: responseMatch[2],
        reason: responseMatch[3],
        version: responseMatch[1]
      };
    }
  }

  parseHeaderLine(line) {
    let match = line.match(/^([^\:]+)\:\s*(.*)$/);
    if(match) {
      return { key: match[1], value: match[2] };
    }
  }
}

class Serializer {

  encode(message) {
    let lines = [];
    if(message.type === 'request') {
      lines.push(message.method.toUpperCase()
                 + ' '
                 + message.uri
                 + ' HTTP/'
                 + (message.version || '1.1'));
    } else {
      lines.push('HTTP/'
                 + (message.version || '1.1')
                 + ' '
                 + message.status
                 + ' '
                 + message.reason);
    }
    message.headers.forEach((header) => {
      lines.push(header.key + ': ' + header.value);
    });
    lines.push('');
    return lines.join('\r\n') + (message.body || '')
  }

}
