import util from '../util';
import eventEmitter from './event_emitter';

export default class TCPSocket {
  constructor(host, port, options) {
    util.extend(this, eventEmitter);

    this.host = host, this.port = port;
    this.hostAndPort = host + ':' + port;
    this.options = options || {};

    var query = this._generateQuery();

    this.socket = new WebSocket(proxyAddress + "/" + this.host + '/' + this.port + query, 'tcp-stream-protocol');

    this.socket.addEventListener('open', () => {
      console.log("Socket opened.");
      this.trigger('open');
    });
    this.socket.addEventListener('message', (message) => {
      console.log('receive', message);
      this.trigger('data', message.data);
    });
    this.socket.addEventListener('close', () => {
      console.log("Connection closed: ", this.hostAndPort);
      this.trigger('close');
    });
  }

  write(data) {
    this.socket.send(data);
  }

  close() {
    this.socket.close();
  }

  _generateQuery() {
    var pairs = [];
    for(var key in this.options) {
      pairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(this.options[key]));
    }
    return pairs.length > 0 ? '?' + pairs.join('&') : '';
  }
}
