import React from 'react';
import BS from 'react-bootstrap';
const {Row, Col} = BS;

import Intro from './widgets/intro.jsx';
import Header from './widgets/header.jsx';
import Connect from './widgets/connect.jsx';
import Response from './widgets/response.jsx';

export default React.createClass({
  getInitialState() {
    this.props.manager.onChange((state) => {
      console.log('will set state from manager', state);
      this.setState(state)
      if(this.refs.response) {
        setTimeout(() => this.refs.response.forceUpdate());
      }
    });
    return this.props.manager.state;
  },

  render() {
    return (
      <div className="container">
        <Row>
          <Col xs={8}>
            <Intro {...this.state.request} onChange={this.requestValueChanged} />
            <Header fields={[{}]} />
          </Col>
          <Col xs={4}>
            <Connect {...this.state.connection}
                     onConnect={this.connect}
                     onDisconnect={this.disconnect} />
          </Col>
        </Row>
        <Row>
          {this.state.response ? <Response {...this.state.response} ref="response" /> : <div/>}
        </Row>
      </div>
    );
  },

  connect() {
    this.props.manager.connect();
  },

  disconnect() {
    this.props.manager.disconnect();
  },

  requestValueChanged(key, value) {
    let update = {};
    update[key] = value;
    this.props.manager.update({ request: update });
  }
});
