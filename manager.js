
class Backend {
  constructor(backendUrl) {
    this.baseUrl = backendUrl;
  }

  get(path, query) {
    return this.request('GET', path, query);
  }

  post(path, query) {
    return this.request('POST', path, query);
  }

  request(method, path, query) {
    return new Promise((fulfill, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.open(method, this.buildUrl(path, query));
      xhr.addEventListener(
        'load', () => fulfill(JSON.parse(xhr.responseText))
      );
      xhr.send();
    });
  }

  buildUrl(path, query) {
    return this.baseUrl + '/' + path + this.encodeQuery(query)
  }

  encodeQuery(query) {
    return query ? '?' + Object.keys(query).map((key) => {
      return encodeURIComponent(key) + '=' +
        encodeURIComponent(query[key]);
    }).join('&') : '';
  }
}

export default class {
  constructor(backendUrl) {
    this.listeners = [];
    this.backend = new Backend(backendUrl);
    // this.state = { method: 'GET', uri: 'http://en.wikipedia.org/wiki/Philosophy' };
    this.state = {
      request: {
        method: 'GET', uri: 'http://localhost:3000/stuff.json'
      }
    };
    this.updateInfo().then((info) => this.updateState({ connection: info }));
  }

  onChange(listener) {
    this.listeners.push(listener);
  }

  connect() {
    return new Promise((fulfill, request) => {
      let connection = new HTTP.Connection()
  }

  send() {
    delete this.state.response;
    return this.backend.post('send', this.state).
      then((response) => this.updateState({ response: response }));
  }

  update(changes) {
    this.updateState(changes);
    this.updateInfo().then((info) => this.updateState({ connection: info }));
  }

  updateInfo() {
    let uri = this.parseURI(this.state.request.uri);
    if(uri) {
      return this.backend.get('uri-info', uri).then((info) => {
        console.log('info', info);
        return info;
      });
    } else {
      return new Promise((f, _) => f({}));
    }
  }

  updateState(values) {
    if(typeof(values.response) === 'object') {
      let response = values.response;
      if(typeof(response.body) === 'string') {
        response.body = this.castBody(
          response.body,
          response.headers ? response.headers['content-type'] : ''
        );
      }
    }
    this.state = this.recursiveMerge(this.state, values);
    this.listeners.forEach((listener) => listener(this.state));
    return this.state;
  }

  castBody(body, type) {
    if(type && type.match(/^application\/json/)) {
      return {
        type: 'application/json',
        data: JSON.parse(body)
      };
    } else {
      return new Blob([body], { type: type });
    }
  }

  loadURIInfo(uri) {
    return this.backend.get('uri-info', uri);
    // return new Promise((f, _) => f(uri));
  }

  parseURI(uri) {
    let match = uri.match(/^([^\:]+)\:(.+)$/), parsed = {};
    if(! match) return parsed;
    parsed.scheme = match[1];
    switch(parsed.scheme) {
    case 'http':
    case 'https':
      {
        match = match[2].match(/^\/\/([^\:\/]+)(?:\:(\d+)|)(.*)$/);
        if(! match) break;
        parsed.host = match[1];
        parsed.port = parseInt(
          match[2] || (parsed.scheme === 'https' ? 443 : 80)
        );
        parsed.path = match[3];
        break;
      }
    }
    return parsed;
  }

  recursiveMerge(a, b) {
    let result = {};
    for(let key in a) {
      result[key] = a[key];
    }
    for(let key in b) {
      if(typeof(result[key]) === 'object' &&
         typeof(b[key]) === 'object' &&
         ((! result[key] instanceof Array) ||
          result[key] === null || b[key] === null)) {
        result[key] = this.recursiveMerge(a, b);
      } else {
        result[key] = b[key];
      }
    }
    return result;
  }
};
