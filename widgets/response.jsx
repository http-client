
import React from 'react';
import {JSONValue} from './json/value.jsx';

export default React.createClass({
  render() {
    console.log('--------------------------------------------------------------------------------');
    console.log('value', JSONValue);
    console.log('--------------------------------------------------------------------------------');
    let body = this.props.body;
    if(! body) {
      return <div />;
    } else if(body.type === 'application/json') {
      console.log('WILL RENDER VALUE', body.data);
      return <JSONValue value={body.data} />;
    } else {
      let url = URL.createObjectURL(this.props.body);
      return <iframe src={url} />;
    }
  },

  shouldComponentUpdate() {
    return false;
  }
});
