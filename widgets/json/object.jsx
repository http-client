
import React from 'react';
import {JSONValue} from './value.jsx';

export const JSONObject = React.createClass({
  render() {
    console.log('VALUE in OBJECT', typeof(JSONValue));
    let object = this.props.object;
    let properties = Object.keys(object).map((key) => {
      return { key: key, value: object[key] };
    });
    return (
      <div className="json-object">
        {properties.map(this.renderProperty)}
      </div>
    );
  },

  renderProperty(property, i) {
    return (
      <div className="json-property" key={i}>
        <div className="json-key">{property.key}</div>
        <div className="json-value">
          <JSONValue value={property.value} />
        </div>
      </div>
    );
  }
});

