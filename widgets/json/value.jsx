
import React from 'react';

export const JSONValue = React.createClass({
  render() {
    let value = this.props.value;
    switch(typeof(value)) {
    case 'undefined':
      return <div className="json-undefined" />;
    case 'object':
      if(value === null) {
        return <div className="json-null" />;
      } else {
        return (
          value instanceof Array ?
            <JSONArray array={value} />
          : <JSONObject object={value} />
        );
      }
    case 'string':
      return <div className="json-string">{value}</div>
    case 'number':
      return <div className="json-number">{value}</div>
    default:
      return <div className="json-unknown">{JSON.stringify(value)}</div>;
    }
  }
});

export const JSONObject = React.createClass({
  render() {
    let object = this.props.object;
    let properties = Object.keys(object).map((key) => {
      return { key: key, value: object[key] };
    });
    return (
      <div className="json-object">
        {properties.map(this.renderProperty)}
      </div>
    );
  },

  renderProperty(property, i) {
    return (
      <div className="json-property row" key={i}>
        <div className="json-key col-xs-4">{property.key}</div>
        <div className="json-value col-xs-8">
          <JSONValue value={property.value} />
        </div>
      </div>
    );
  }
});

export const JSONArray = React.createClass({
  render() {
    return (
      <div className="json-array container">
        {this.props.array.map(this.renderItem)}
      </div>
    );
  },

  renderItem(item, i) {
    return (
      <div className="json-item col-xs-10 col-xs-offset-2 row" key={i}>
        <JSONValue value={item} />
      </div>
    );
  }
});

