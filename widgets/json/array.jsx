
import React from 'react';
import {JSONValue} from './value.jsx';

export const JSONArray = React.createClass({
  render() {
    console.log('VALUE in ARRAY:', typeof(JSONValue));
    return (
      <div className="json-array">
        {this.props.array.map(this.renderItem)}
      </div>
    );
  },

  renderItem(item, i) {
    return (
      <div className="json-item" key={i}>
        <JSONValue value={item} />
      </div>
    );
  }
});
