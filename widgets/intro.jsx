
import React from 'react';
import BS from 'react-bootstrap';
const {Input, Row, Col} = BS;

const METHODS = [ 'GET', 'PUT', 'POST', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS' ];

export default React.createClass({
  render() {
    return (
      <Row>
        <Col xs={4}>
          <Input onKeyDown={this.methodChanged}
                 defaultValue={this.props.method}
                 type="text"
                 ref="method"
                 placeholder="Method" />
        </Col>
        <Col xs={8}>
          <Input onChange={this.uriChanged}
                 defaultValue={this.props.uri}
                 type="text"
                 ref="uri"
                 placeholder="URI" />
        </Col>
      </Row>
    );
  },

  methodChanged(event) {
    if(event.which === 8) return;
    setTimeout(() => {
      let method = this.refs.method.getValue(), value;
      let completions = METHODS.filter((m) => m.indexOf(method) === 0);
      if(completions.length === 1) {
        value = completions[0];
        if(value === method) {
          this.props.onChange('method', method);
        } else {
          let node = this.refs.method.getInputDOMNode();
          node.value = value;
          node.setSelectionRange(method.length, value.length);
        }
      } else {
        value = method;
      }
    });
  },

  shouldComponentUpdate() {
    return false;
  },

  uriChanged() {
    this.props.onChange('uri', this.refs.uri.getValue());
  },
});
