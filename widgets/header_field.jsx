
const React = require('react');
const {Input, Col, Row} = require('react-bootstrap');

export default React.createClass({
  render() {
    return (
      <Row>
        <Col xs={4}><Input type="text" ref="key" /></Col>
        <Col xs={8}><Input type="text" ref="value" /></Col>
      </Row>
    );
  }
});

