
import React from 'react';
import BS from 'react-bootstrap';
const {Panel, ListGroup, ListGroupItem, Input, Button} = BS;

export default React.createClass({
  render() {
    console.log('render connect', this.props);
    let addresses = this.props.addresses || [];
    let inputProps = {
      labelClassName: 'col-xs-4',
      inputClassName: 'col-xs-8',
      type: 'static',
    };
    return (
      <Panel header="Connect">
        <Input {...inputProps} label="Scheme" value={this.props.scheme} />
        <Input {...inputProps} label="Port" value={this.props.port} />
        <ListGroup>
          {addresses.map(this.renderAddress)}
        </ListGroup>
        { this.props.connected ?
            <Button type="default" onClick={this.props.onDisconnect}>
              Disconnect
            </Button>
          : <Button type="primary" onClick={this.props.onConnect}>
              Connect
            </Button> }
      </Panel>
    );
  },

  renderAddress(address, index) {
    return <ListGroupItem active={index === 0} key={index}>{address}</ListGroupItem>
  }
});
