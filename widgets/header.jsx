
const React = require('react');

import HeaderField from './header_field.jsx';

export default React.createClass({
  render() {
    return (
      <div className="header">
        {this.props.fields.map(this.renderField)}
      </div>
    );
  },

  renderField(field) {
    return <HeaderField {...field} />
  }
});
